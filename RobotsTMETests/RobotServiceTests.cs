using Moq;
using RobotsTME.AppModels;
using RobotsTME.Models;
using RobotsTME.Repository;
using RobotsTME.Service;
using NUnit.Framework.Legacy;

namespace RobotsTMETests
{
    [TestFixture]
    public class RobotServiceTests
    {
        private IRobotService _robotService;
        private Mock<IRobotRepository> _mockRepository;

        [SetUp]
        public void Setup()
        {
            _mockRepository = new Mock<IRobotRepository>();
            _robotService = new RobotService(_mockRepository.Object);
        }

        [Test]
        public void GetRobots_ReturnsRobots()
        {
            // Arrange
            List<Robot> expectedRobots = new List<Robot>
            {
                new Robot
                {
                    Name = "Chiron",
                    Icon = "/RobotsTME;component/Resources/robot_1_black.png",
                    BatteryLevel = 70,
                    Location = "USM 201",
                    Position = Position.Safe,
                    Status = Status.DuringTheAction,
                    DockStatus = DockStatus.ReadyToWork,
                    JobStatus = JobStatus.InAction
                },
                new Robot
                {
                    Name = "Robot 1",
                    Icon = "/RobotsTME;component/Resources/robot_2_black.png",
                    BatteryLevel = 70,
                    Location = "USM 201",
                    Position = Position.Safe,
                    Status = Status.UnavailableBusy,
                    DockStatus = DockStatus.Docked,
                    JobStatus = JobStatus.InAction
                }
            };
            _mockRepository.Setup(r => r.GetRobots()).Returns(expectedRobots);

            // Act
            var result = _robotService.GetRobots();

            // Assert
            ClassicAssert.AreEqual(expectedRobots, result);
        }

        [Test]
        public void ChangeEnergyAndStatus_UpdatesRobot()
        {
            // Arrange
            var robot = new RobotAppModel(new Robot
            {
                Name = "Robot 3",
                Icon = "/RobotsTME;component/Resources/robot_4_black.png",
                BatteryLevel = 70,
                Location = "USM 201",
                Position = Position.Safe,
                Status = Status.NeedsAssistance,
                DockStatus = DockStatus.EStopReleased,
                JobStatus = JobStatus.InAction
            });

            // Act
            _robotService.ChangeEnergyAndStatus(robot);

            // Assert
            ClassicAssert.AreEqual(Status.DuringTheAction, robot.Status);
            ClassicAssert.AreEqual(80, robot.BatteryLevel);
            _mockRepository.Verify(r => r.Update(robot.Robot), Times.Once);
        }

        [Test]
        public void ChangeEnergyAndStatus_IncreasesToMax100_WhenEnergyIsIncreasedAbove100()
        {
            // Arrange
            var robot = new RobotAppModel(new Robot
            {
                Name = "Robot 3",
                Icon = "/RobotsTME;component/Resources/robot_4_black.png",
                BatteryLevel = 95,
                Location = "USM 201",
                Position = Position.Safe,
                Status = Status.NeedsAssistance,
                DockStatus = DockStatus.EStopReleased,
                JobStatus = JobStatus.InAction
            });

            // Act
            _robotService.ChangeEnergyAndStatus(robot);

            // Assert
            ClassicAssert.AreEqual(Status.DuringTheAction, robot.Status);
            ClassicAssert.AreEqual(100, robot.BatteryLevel);

            _mockRepository.Verify(r => r.Update(robot.Robot), Times.Once);
        }
    }
}