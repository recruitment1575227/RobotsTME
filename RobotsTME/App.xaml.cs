﻿using System.Windows;

namespace RobotsTME
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //var sqlRobotService = new SqlRobotService();
            //sqlRobotService.InitializeDatabaseWithMockData();

            //var mainWindow = new MainWindow(sqlRobotService);
            //mainWindow.Show();
            StartupUri = new Uri("Views/MainWindow.xaml", UriKind.Relative);
        }
    }

}
