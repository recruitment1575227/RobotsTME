﻿using RobotsTME.DBContexts;
using RobotsTME.Models;

namespace RobotsTME.Repository
{
    public class SqlRobotRepository : IRobotRepository
    {
        private readonly RobotDbContext _dbContext;

        public SqlRobotRepository()
        {
            _dbContext = new RobotDbContext();
            _dbContext.Database.EnsureCreated();
        }

        public List<Robot> GetRobots()
        {
            return _dbContext.Robots.ToList();
        }

        public void InitializeData()
        {
            if (!_dbContext.Robots.Any())
            {
                var mockService = new MockRobotRepository();
                var robots = mockService.GetRobots();

                _dbContext.Robots.AddRange(robots);
                _dbContext.SaveChanges();
            }
        }

        public void Update(Robot robot)
        {
            _dbContext.Update(robot);
            _dbContext.SaveChanges();
        }
    }
}
