﻿using RobotsTME.Models;

namespace RobotsTME.Repository
{
    public interface IRobotRepository
    {
        List<Robot> GetRobots();
        void InitializeData();
        void Update(Robot robot);
    }
}
