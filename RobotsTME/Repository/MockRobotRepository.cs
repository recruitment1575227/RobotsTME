﻿using RobotsTME.Models;

namespace RobotsTME.Repository
{
    public class MockRobotRepository : IRobotRepository
    {
        private List<Robot> _robots;

        public List<Robot> GetRobots()
        {
            return _robots;
        }

        public void InitializeData()
        {
            _robots = new List<Robot>
            {
                new Robot
                {
                    Name = "Chiron",
                    Icon = "/RobotsTME;component/Resources/robot_1_black.png",
                    BatteryLevel = 70,
                    Location = "USM 201",
                    Position = Position.Safe,
                    Status = Status.DuringTheAction,
                    DockStatus = DockStatus.ReadyToWork,
                    JobStatus = JobStatus.InAction
                },
                new Robot
                {
                    Name = "Robot 1",
                    Icon = "/RobotsTME;component/Resources/robot_2_black.png",
                    BatteryLevel = 70,
                    Location = "USM 201",
                    Position = Position.Safe,
                    Status = Status.UnavailableBusy,
                    DockStatus = DockStatus.Docked,
                    JobStatus = JobStatus.InAction
                },
                new Robot
                {
                    Name = "Robot 2",
                    Icon = "/RobotsTME;component/Resources/robot_3_black.png",
                    BatteryLevel = 70,
                    Location = "USM 201",
                    Position = Position.Safe,
                    Status = Status.Available,
                    DockStatus = DockStatus.ReadyToWork,
                    JobStatus = JobStatus.InAction
                },
                new Robot
                {
                    Name = "Robot 3",
                    Icon = "/RobotsTME;component/Resources/robot_4_black.png",
                    BatteryLevel = 70,
                    Location = "USM 201",
                    Position = Position.Safe,
                    Status = Status.NeedsAssistance,
                    DockStatus = DockStatus.EStopReleased,
                    JobStatus = JobStatus.InAction
                }
            };
        }

        public void Update(Robot robot)
        {
            //Mock
        }
    }
}
