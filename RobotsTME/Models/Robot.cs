﻿namespace RobotsTME.Models
{
    public class Robot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public int BatteryLevel { get; set; }
        public string Location { get; set; }
        public Position Position { get; set; }
        public Status Status { get; set; }
        public DockStatus DockStatus { get; set; }
        public JobStatus JobStatus { get; set; }
    }

    public enum Position
    {
        Safe
    }

    public enum Status
    {
        DuringTheAction,
        Available,
        UnavailableBusy,
        NeedsAssistance
    }

    public enum DockStatus
    {
        ReadyToWork,
        Docked,
        EStopReleased
    }

    public enum JobStatus
    {
        InAction
    }
}
