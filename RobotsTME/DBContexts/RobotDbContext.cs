﻿using Microsoft.EntityFrameworkCore;
using RobotsTME.Models;

namespace RobotsTME.DBContexts
{
    public class RobotDbContext : DbContext
    {
        public DbSet<Robot> Robots { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Server=DESKTOP-GHJB719;Database=RobotDatabase2;Trusted_Connection=True;TrustServerCertificate=True;";
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
