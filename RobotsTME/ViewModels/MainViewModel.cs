﻿using GalaSoft.MvvmLight.Command;
using RobotsTME.AppModels;
using RobotsTME.Models;
using RobotsTME.Repository;
using RobotsTME.Service;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace RobotsTME.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IRobotService _robotService;
        private List<Robot> _allRobots;
        private int _selectedRobotIndex;

        public ObservableCollection<string> RobotNames { get; set; }
        public ObservableCollection<RobotAppModel> DisplayedRobots { get; set; }

        public int SelectedRobotIndex
        {
            get { return _selectedRobotIndex; }
            set
            {
                _selectedRobotIndex = value;
                UpdateDisplayedRobots();
                OnPropertyChanged(nameof(SelectedRobotIndex));
            }
        }

        public ICommand LoadRobotsCommand { get; private set; }
        public ICommand ChangeStatusAndEnergyCommand { get; private set; }
        public ICommand PreviousRobotCommand { get; private set; }
        public ICommand NextRobotCommand { get; private set; }

        public MainViewModel()
        {
            _robotService = new RobotService(new SqlRobotRepository());

            _allRobots = new List<Robot>();

            RobotNames = new ObservableCollection<string>();
            RobotNames.Insert(0, "Wszystkie");

            DisplayedRobots = new ObservableCollection<RobotAppModel>();

            LoadRobotsCommand = new RelayCommand(LoadRobots);
            ChangeStatusAndEnergyCommand = new RelayCommand(ChangeStatusAndEnergy);
            PreviousRobotCommand = new RelayCommand(PreviousRobot);
            NextRobotCommand = new RelayCommand(NextRobot);

            SelectedRobotIndex = 0;
        }

        private void LoadRobots()
        {
            _allRobots = _robotService.GetRobots();

            RobotNames = new ObservableCollection<string>();
            RobotNames.Insert(0, "Wszystkie");
            foreach (var robot in _allRobots)
            {
                RobotNames.Add(robot.Name);
            } 

            DisplayedRobots = new ObservableCollection<RobotAppModel>(_allRobots.Select(robot => new RobotAppModel(robot)));

            SelectedRobotIndex = 0;
            OnPropertyChanged(nameof(RobotNames));
        }

        public void ChangeStatusAndEnergy()
        {
            foreach (var robot in DisplayedRobots)
            {
                _robotService.ChangeEnergyAndStatus(robot);
            }
        }

        private void PreviousRobot()
        {
            if (SelectedRobotIndex > 0)
                SelectedRobotIndex--;
        }

        private void NextRobot()
        {
            if (SelectedRobotIndex < RobotNames.Count-1)
                SelectedRobotIndex++;
        }

        private void UpdateDisplayedRobots()
        {
            if (_allRobots != null)
                if (SelectedRobotIndex == 0)
                    DisplayedRobots = new ObservableCollection<RobotAppModel>( _allRobots.Select(robot => new RobotAppModel(robot)) );
                else
                    DisplayedRobots = new ObservableCollection<RobotAppModel>( _allRobots.Skip(SelectedRobotIndex-1).Take(1).Select(robot => new RobotAppModel(robot)) );
            else
                DisplayedRobots = new ObservableCollection<RobotAppModel>();

            OnPropertyChanged(nameof(DisplayedRobots));
        }
    }
}
