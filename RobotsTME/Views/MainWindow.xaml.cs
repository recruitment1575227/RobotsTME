﻿using RobotsTME.ViewModels;
using System.Windows;

namespace RobotsTME.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }
    }
}