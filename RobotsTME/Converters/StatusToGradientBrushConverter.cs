﻿using RobotsTME.Models;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace RobotsTME.Converters
{
    public class StatusToGradientBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Status status)
            {
                switch (status)
                {
                    case Status.DuringTheAction:
                        return CreateGradientBrush((Color)ColorConverter.ConvertFromString("#1A93F8"), Colors.White);
                    case Status.Available:
                        return CreateGradientBrush((Color)ColorConverter.ConvertFromString("#00E12D"), Colors.White);
                    case Status.UnavailableBusy:
                        return CreateGradientBrush((Color)ColorConverter.ConvertFromString("#FDB500"), Colors.White);
                    case Status.NeedsAssistance:
                        return CreateGradientBrush((Color)ColorConverter.ConvertFromString("#F80016"), Colors.White);
                }
            }
            return CreateGradientBrush((Color)ColorConverter.ConvertFromString("#F80016"), Colors.White);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private LinearGradientBrush CreateGradientBrush(Color color1, Color color2)
        {
            var gradientBrush = new LinearGradientBrush();
            gradientBrush.StartPoint = new System.Windows.Point(0, 0);
            gradientBrush.EndPoint = new System.Windows.Point(1, 0);
            gradientBrush.GradientStops.Add(new GradientStop(color1, 0));
            gradientBrush.GradientStops.Add(new GradientStop(color1, 0.015));
            gradientBrush.GradientStops.Add(new GradientStop(color2, 0.015));
            gradientBrush.GradientStops.Add(new GradientStop(color2, 1));

            return gradientBrush;
        }
    }
}
