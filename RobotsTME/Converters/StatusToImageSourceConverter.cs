﻿using RobotsTME.Models;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace RobotsTME.Converters
{
    public class StatusToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Status status)
            {
                string imagePath;

                // Wybierz odpowiednie źródło obrazu na podstawie wartości statusu
                switch (status)
                {
                    case Status.DuringTheAction:
                        imagePath = "/RobotsTME;component/Resources/circle.jpg";
                        break;
                    case Status.Available:
                        imagePath = "/RobotsTME;component/Resources/check.png";
                        break;
                    case Status.UnavailableBusy:
                        imagePath = "/RobotsTME;component/Resources/warning.png";
                        break;
                    case Status.NeedsAssistance:
                        imagePath = "/RobotsTME;component/Resources/error.png";
                        break;
                    default:
                        imagePath = "/RobotsTME;component/Resources/error.png";
                        break;
                }

                return new BitmapImage(new Uri(imagePath, UriKind.Relative));
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
