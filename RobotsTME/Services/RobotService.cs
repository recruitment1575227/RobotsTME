﻿using RobotsTME.AppModels;
using RobotsTME.Models;
using RobotsTME.Repository;

namespace RobotsTME.Service
{
    public class RobotService : IRobotService
    {
        private IRobotRepository _robotRepository;

        public RobotService(IRobotRepository robotRepository)
        {
            _robotRepository = robotRepository ?? throw new ArgumentNullException(nameof(robotRepository));
            robotRepository.InitializeData();

        }

        public List<Robot> GetRobots()
        {
            return _robotRepository.GetRobots();
        }

        public void ChangeEnergyAndStatus(RobotAppModel robot)
        {
            robot.Status = (Status)(((int)robot.Status + 1) % Enum.GetValues(typeof(Status)).Length);
            robot.BatteryLevel = Math.Min(robot.BatteryLevel + 10, 100);
            _robotRepository.Update(robot.Robot);
        }
    }
}
