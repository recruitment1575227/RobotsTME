﻿using RobotsTME.AppModels;
using RobotsTME.Models;

namespace RobotsTME.Service
{
    public interface IRobotService
    {
        List<Robot> GetRobots();
        void ChangeEnergyAndStatus(RobotAppModel robot);
    }
}
