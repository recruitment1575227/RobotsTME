﻿using RobotsTME.Models;

namespace RobotsTME.AppModels
{
    public class RobotAppModel : BaseModel
    {
        public Robot Robot { get; }

        public RobotAppModel(Robot robot)
        {
            Robot = robot;
        }

        public string Name
        {
            get => Robot.Name;
            set => Robot.Name = value;
        }

        public string Icon
        {
            get => Robot.Icon;
            set => Robot.Icon = value;
        }

        public int BatteryLevel
        {
            get => Robot.BatteryLevel;
            set
            {
                Robot.BatteryLevel = value;
                OnPropertyChanged(nameof(Robot.BatteryLevel));
            }
        }

        public string Location
        {
            get => Robot.Location;
            set => Robot.Location = value;
        }

        public Position Position
        {
            get => Robot.Position;
            set => Robot.Position = value;
        }

        public Status Status
        {
            get => Robot.Status;
            set
            {
                Robot.Status = value;
                OnPropertyChanged(nameof(Robot.Status));
            }
        }

        public DockStatus DockStatus
        {
            get => Robot.DockStatus;
            set => Robot.DockStatus = value;
        }

        public JobStatus JobStatus
        {
            get => Robot.JobStatus;
            set => Robot.JobStatus = value;
        }
    }
}
